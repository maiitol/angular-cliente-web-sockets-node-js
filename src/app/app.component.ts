
import { Component,OnDestroy } from '@angular/core';
import  { WebsocketService } from './websocket.service';

@Component({
  selector: 'app-root',
  //templateUrl: './app.component.html',
  providers: [ WebsocketService ],
  template: `<h1 style="display:inline-block;">Cliente que ha iniciado sesi�n: {{ user }}</h1>
  <i class="material-icons" style="font-size: 40px; margin-left: 26px;    cursor: pointer;">notifications_none</i>
  <div style="     cursor: pointer;   display: inline-block;  border-radius: 100%;  background: red;  height: 15px;  width: 15px;  color: white;position: relative;  left: -16px;  top: -4px;"><b style="    position: relative;  left: 4px;  top: -3px;  font-size: 13px;">{{ numberNotifi }}</b></div>
  <div style="        background: white;position: absolute;  left: 59%;  border: 1px solid black;  border-radius: 3px;  padding: 5px;  width: 260px;  top: 58px;">
    <p>Notificaciones</p>
    <div *ngFor="let item of messageArray; index as i;" style="    border: 1px solid black;    border-radius: 4px;">
      <p style="    margin-to: 4px;margin-bottom:0px;">Topic: {{ item.topic }}</p>
      <p style="    margin-top: 4px;margin-bottom:0px;">Usuario: {{ item.nombre }}</p>
      <p style="    margin-top: 4px;margin-bottom:0px;"><b>Mensaje: {{ item.mensaje }}</b></p>
    </div>
  </div>
  <br><br>
  
  <br>
  <button (click)="suscripcionNotificaciones()">Crear un nuevo contrato</button>
  <br>
  
  <br>
  <hr>
  Mis Topicos
  <ul>
    <li *ngFor="let item of topicsA; index as i;">
      {{ item }} <button (click)="eliminarSuscripcionTopic(item)">Eliminar suscripci�n</button>
    </li>
  </ul>
  `
})


export class AppComponent implements OnDestroy {

    user:String;
    id:String;
    topics:String;
    topic:String;
    messageText:String;
    numberNotifi:Number;
    messageArray:Array<{nombre:String,topic:String,mensaje:String}> = [];
    topicsA:Array<{}> = [];

  constructor(private _wsService: WebsocketService) {
    
    this.numberNotifi = 0;


    //Recibe los mensaje generales del servidor
    this._wsService.crearMensaje()
        .subscribe(data=> {
          console.log("---- Nuevo mensaje del servidor ---");
          console.log(data);
        });

    this._wsService.listarUsuarioConectado()
    .subscribe( (...data: any[]) => {
      console.log("---- Usuarios activos topico " + data[0]["topic"] + ":" + data[0].usuariosTopic.length + " --- ");
      console.log(data[0].usuariosTopic);
      //
      if(data[0].topicos != undefined)
      {
        this.topicsA = data[0].topicos
      }
        
    })

    this._wsService.mensajePrivado()
    .subscribe(data=> {
      console.log("---- Mensaje privado ---");
      console.log(data);
    })

    this._wsService.mensajeAprobacion()
    .subscribe((...data: any[]) => {

      this.numberNotifi = +this.numberNotifi + 1;

      this.messageArray.push({
        nombre : data[0]["usuario"],
        topic:data[0]["topic"],
        mensaje:data[0]["mensaje"]
      });

      console.log(this.messageArray);
      
    })

    this.join();
  }

  join(){
    //Realizar conexi�n con el Sockets y un topico
    this.user = "Cliente_" + Math.floor((Math.random() * 100000) + 1);
    this.topic = "topicGeneral";
    this._wsService.joinConnection({nombre:this.user, topic:this.topic, type: 'client'})
    .subscribe((...data: any[])=>{
        console.log("-- Usuarios conectados al topic " + this.topic + " : " + data[0].length  +"--");
        console.log(data);
        this.topicsA = data[0].topicos
    });
  }

  leave(){
      this._wsService.leaveChannel({user:this.user, room:this.topic});
  }

  sendMessagePrivate()
  {
      this._wsService.sendMessagePrivate({para:this.id, mensaje:this.messageText});
  }

  //Suscripci�n de notificaciones- Crear nuevo contrato
  suscripcionNotificaciones()
  {
    this._wsService.joinConnection({nombre:this.user, topic:'topicCreacionContratos', type: 'client'})
    .subscribe((...data: any[])=>{
        console.log("-- Usuarios conectados al topic topicCreacionContratos : " + data[0].length  + "--");
        console.log(data);
        this.topicsA = data[0].topicos
    });
  }

  //Suscripci�n de notificaciones- Actualizar nuevo contrato
  suscripcionNotificacionesNew()
  {
    this._wsService.joinConnection({nombre:this.user, topic:'topicNewEvent', type: 'client'})
    .subscribe((...data: any[])=>{
        console.log("-- Usuarios conectados al topic topicNewEvent : " + data[0].length  + "--");
        console.log(data);
    });
  }

  //Eliminar suscripci�n a topicos
  eliminarSuscripcionTopic(topic)
  {
    this._wsService.deleteSuscripcion(topic)
      .subscribe((...data: any[])=>{
        this.topicsA = data[0].topicos
    });
  }

  closeSocket(){
    this._wsService.close();
    //this.wsSubscription.unsubscribe();  // Close the Websocket
    //this.status = 'The socket is closed';
  }

  ngOnDestroy() {
    this.closeSocket();
  }
}